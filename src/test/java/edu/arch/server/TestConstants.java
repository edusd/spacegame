package edu.arch.server;

import edu.arch.server.dao.UObject;
import edu.arch.server.dao.UObjectHashMap;
import edu.arch.server.domain.graph.Direction;
import edu.arch.server.domain.graph.Vector;

import java.util.Map;

public class TestConstants {
    public static Map<String, Object> properties() {
        return Map.of(
                "Position", new Vector(0d, 0d),
                "Velocity", 10,
                "Direction", new Direction(0, 6),
                "AngularVelocity", 2,
                "Fuel", 10,
                "BurnFuelRate", 2
        );
    }

    public static UObject uObject() {
        UObject o = new UObjectHashMap();
        o.setProperty("Name", "uObjectTest");
        properties().forEach(o::setProperty);
        return o;
    }

    private TestConstants() {}
}
