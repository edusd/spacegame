package edu.arch.server;

import edu.arch.server.commands.MoveCommand;
import edu.arch.server.commands.RotateCommand;
import edu.arch.server.dao.UObject;
import edu.arch.server.domain.adapter.FuelBurnableObject;
import edu.arch.server.domain.adapter.MovableObject;
import edu.arch.server.domain.adapter.RotableObject;
import edu.arch.server.domain.graph.Direction;
import edu.arch.server.domain.graph.Vector;
import edu.arch.server.exceptions.CommandException;
import edu.arch.server.exceptions.CommandRetryException;
import edu.arch.server.exceptions.CommandRetryExpireException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;

import java.util.Collections;
import java.util.Optional;

class GameServerTest {
    ExceptionHandler exceptionHandler;
    GameServer server;

    @BeforeEach
    void init() {
        exceptionHandler = Mockito.spy(ExceptionHandler.class);
        server = new GameServer(exceptionHandler);
    }

    @Test
    void movement() {
        server.spawn("SpaceShip", TestConstants.properties());
        Assertions.assertDoesNotThrow(() -> server.movement());
        Optional<UObject> spaceShip = Assertions.assertDoesNotThrow(() -> server.get("SpaceShip"));
        Assertions.assertTrue(spaceShip.isPresent());
        Assertions.assertEquals(0, spaceShip.map(FuelBurnableObject::new).get().getFuel());
        Assertions.assertEquals(
                new Vector(50d, 0d),
                spaceShip.map(MovableObject::new).get().getPosition()
        );
    }

    @Test
    void move() {
        server.spawn("SpaceShip", TestConstants.properties());
        Assertions.assertDoesNotThrow(() -> server.move());
        Optional<UObject> spaceShip = Assertions.assertDoesNotThrow(() -> server.get("SpaceShip"));
        Assertions.assertTrue(spaceShip.isPresent());
        Assertions.assertEquals(
                new Vector(10d, 0d),
                spaceShip.map(MovableObject::new).get().getPosition()
        );
    }

    @Test
    void moveFailureWithRetry() {
        server.spawn("SpaceShip", Collections.emptyMap());
        Assertions.assertDoesNotThrow(() -> server.move());
        Mockito.verify(exceptionHandler).handle(ArgumentMatchers.any(MoveCommand.class), ArgumentMatchers.any(CommandException.class));
        Mockito.verify(exceptionHandler).handle(ArgumentMatchers.any(MoveCommand.class), ArgumentMatchers.any(CommandRetryException.class));
        Mockito.verify(exceptionHandler).handle(ArgumentMatchers.any(MoveCommand.class), ArgumentMatchers.any(CommandRetryException.class));
        Mockito.verify(exceptionHandler).handle(ArgumentMatchers.any(MoveCommand.class), ArgumentMatchers.any(CommandRetryExpireException.class));
    }

    @Test
    void rotate() {
        server.spawn("SpaceShip", TestConstants.properties());
        Assertions.assertDoesNotThrow(() -> server.rotate());
        Optional<UObject> spaceShip = Assertions.assertDoesNotThrow(() -> server.get("SpaceShip"));
        Assertions.assertTrue(spaceShip.isPresent());
        Assertions.assertEquals(
                new Direction(2, 6),
                spaceShip.map(RotableObject::new).get().getDirection()
        );
    }

    @Test
    void rotateFailureWithRetry() {
        server.spawn("SpaceShip", Collections.emptyMap());
        Assertions.assertDoesNotThrow(() -> server.rotate());
        Mockito.verify(exceptionHandler).handle(ArgumentMatchers.any(RotateCommand.class), ArgumentMatchers.any(CommandException.class));
        Mockito.verify(exceptionHandler).handle(ArgumentMatchers.any(RotateCommand.class), ArgumentMatchers.any(CommandRetryException.class));
        Mockito.verify(exceptionHandler).handle(ArgumentMatchers.any(RotateCommand.class), ArgumentMatchers.any(CommandRetryException.class));
        Mockito.verify(exceptionHandler).handle(ArgumentMatchers.any(RotateCommand.class), ArgumentMatchers.any(CommandRetryExpireException.class));
    }
}
