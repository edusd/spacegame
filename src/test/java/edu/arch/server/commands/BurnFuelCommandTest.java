package edu.arch.server.commands;

import edu.arch.server.TestConstants;
import edu.arch.server.dao.UObject;
import edu.arch.server.domain.adapter.FuelBurnableObject;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class BurnFuelCommandTest {
    @Test
    void execute() {
        UObject o = TestConstants.uObject();
        o.setProperty("Fuel", 10);
        o.setProperty("BurnFuelRate", 4);
        FuelBurnableObject fb = new FuelBurnableObject(o);
        Command cmd = new BurnFuelCommand(fb);
        Assertions.assertDoesNotThrow(cmd::execute);
        Assertions.assertEquals(6, new FuelBurnableObject(o).getFuel());
    }
}
