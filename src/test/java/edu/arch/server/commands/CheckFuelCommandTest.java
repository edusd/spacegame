package edu.arch.server.commands;

import edu.arch.server.TestConstants;
import edu.arch.server.dao.UObject;
import edu.arch.server.domain.adapter.FuelBurnableObject;
import edu.arch.server.exceptions.CommandException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class CheckFuelCommandTest {
    @Test
    void executeSuccess() {
        UObject o = TestConstants.uObject();
        FuelBurnableObject fb = new FuelBurnableObject(o);
        Command cmd = new CheckFuelCommand(fb);
        Assertions.assertDoesNotThrow(cmd::execute);
    }

    @Test
    void executeFailure() {
        UObject o = TestConstants.uObject();
        o.setProperty("Fuel", 0);
        FuelBurnableObject fb = new FuelBurnableObject(o);
        Command cmd = new CheckFuelCommand(fb);
        Assertions.assertThrows(CommandException.class, cmd::execute);
    }
}
