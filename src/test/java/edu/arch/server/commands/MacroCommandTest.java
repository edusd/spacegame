package edu.arch.server.commands;

import edu.arch.server.TestConstants;
import edu.arch.server.dao.UObject;
import edu.arch.server.domain.adapter.FuelBurnableObject;
import edu.arch.server.domain.adapter.MovableObject;
import edu.arch.server.domain.graph.Vector;
import edu.arch.server.exceptions.CommandException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.List;

class MacroCommandTest {
    @Test
    void executeSuccess() {
        UObject o = TestConstants.uObject();
        Command cmd = new MacroCommand(List.of(
                new CheckFuelCommand(new FuelBurnableObject(o)),
                new MoveCommand(new MovableObject(o)),
                new BurnFuelCommand(new FuelBurnableObject(o)))
        );
        Assertions.assertDoesNotThrow(cmd::execute);
        Assertions.assertEquals(8, new FuelBurnableObject(o).getFuel());
        Assertions.assertEquals(
                new Vector(10d, 0d),
                new MovableObject(o).getPosition()
        );
    }

    @Test
    void executeFailure() {
        UObject o = TestConstants.uObject();
        o.setProperty("Fuel", 0);
        Command cmd = new MacroCommand(List.of(
                new CheckFuelCommand(new FuelBurnableObject(o)),
                new MoveCommand(new MovableObject(o)),
                new BurnFuelCommand(new FuelBurnableObject(o)))
        );
        Assertions.assertThrows(CommandException.class, cmd::execute);
        Assertions.assertEquals(
                new Vector(0d, 0d),
                new MovableObject(o).getPosition()
        );
    }
}
