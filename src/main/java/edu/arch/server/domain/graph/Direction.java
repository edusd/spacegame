package edu.arch.server.domain.graph;

public record Direction(Integer value, Integer directionsNumber) {
    public Direction(Integer value, Integer directionsNumber){
        this.value = value % directionsNumber;
        this.directionsNumber = directionsNumber;
    }

    public static Direction next(Direction d, Integer angularVelocity) {
        return new Direction(
                (d.value() + angularVelocity) % d.directionsNumber(),
                d.directionsNumber()
        );
    }
}
