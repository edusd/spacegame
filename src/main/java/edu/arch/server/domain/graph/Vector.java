package edu.arch.server.domain.graph;

public record Vector(Double x, Double y) {
    public static Vector plus(Vector v1, Vector v2) {
        return new Vector(
                v1.x() + v2.x(),
                v1.y() + v2.y()
        );
    }
}
