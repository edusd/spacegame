package edu.arch.server.domain.interfaces;

public interface FuelBurnable {
    void setFuel(int fuel);
    int getFuel();

    int getBurnFuelRate();
}
