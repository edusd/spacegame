package edu.arch.server.domain.interfaces;

import edu.arch.server.domain.graph.Direction;

public interface Rotable {
    Direction getDirection();
    int getAngularVelocity();
    void setDirection(Direction newDirection);
}
