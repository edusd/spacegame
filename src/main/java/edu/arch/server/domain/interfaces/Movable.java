package edu.arch.server.domain.interfaces;

import edu.arch.server.domain.graph.Vector;

public interface Movable {
    Vector getPosition();
    Vector getVelocity();
    void setPosition(Vector newPosition);
}
