package edu.arch.server.domain.adapter;

import edu.arch.server.dao.UObject;
import edu.arch.server.domain.graph.Direction;
import edu.arch.server.domain.graph.Vector;
import edu.arch.server.domain.interfaces.Movable;

public class MovableObject implements Movable {
    private final UObject o;

    public MovableObject(UObject o) {
        this.o = o;
    }

    @Override
    public Vector getPosition() {
        return (Vector) o.getProperty("Position");
    }

    @Override
    public Vector getVelocity() {
        int d = ((Direction) o.getProperty("Direction")).value();
        int n = ((Direction) o.getProperty("Direction")).directionsNumber();
        int v = (Integer) o.getProperty("Velocity");
        return new Vector(
                v * Math.cos((double) d*360/n),
                v * Math.sin((double) d*360/n)
        );
    }

    @Override
    public void setPosition(Vector newPosition) {
        o.setProperty("Position", newPosition);
    }
}
