package edu.arch.server.domain.adapter;

import edu.arch.server.dao.UObject;
import edu.arch.server.domain.interfaces.FuelBurnable;

public class FuelBurnableObject implements FuelBurnable {
    private final UObject o;

    public FuelBurnableObject(UObject o) {
        this.o = o;
    }

    @Override
    public void setFuel(int fuel) {
        o.setProperty("Fuel", fuel);
    }

    @Override
    public int getFuel() {
        return (int) o.getProperty("Fuel");
    }

    @Override
    public int getBurnFuelRate() {
        return (int) o.getProperty("BurnFuelRate");
    }
}
