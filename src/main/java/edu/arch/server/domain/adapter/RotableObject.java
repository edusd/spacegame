package edu.arch.server.domain.adapter;

import edu.arch.server.dao.UObject;
import edu.arch.server.domain.graph.Direction;
import edu.arch.server.domain.interfaces.Rotable;

public class RotableObject implements Rotable {
    private final UObject o;

    public RotableObject(UObject o) {
        this.o = o;
    }

    @Override
    public Direction getDirection() {
        return (Direction) o.getProperty("Direction");
    }

    @Override
    public int getAngularVelocity() {
        return (int) o.getProperty("AngularVelocity");
    }

    @Override
    public void setDirection(Direction newDirection) {
        o.setProperty("Direction", newDirection);
    }
}
