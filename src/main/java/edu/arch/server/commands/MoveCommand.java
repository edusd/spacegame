package edu.arch.server.commands;

import edu.arch.server.domain.graph.Vector;
import edu.arch.server.domain.interfaces.Movable;
import edu.arch.server.exceptions.CommandException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class MoveCommand implements Command {
    private final Logger log = LoggerFactory.getLogger(MoveCommand.class.getName());
    private final Movable m;
    public MoveCommand(Movable m) {
        this.m = m;
    }

    @Override
    public void execute() {
        try {
            m.setPosition(Vector.plus(m.getPosition(), m.getVelocity()));
            log.info("Moved to {}", m.getPosition());
        } catch (RuntimeException ex) {
            throw new CommandException(ex);
        }
    }
}
