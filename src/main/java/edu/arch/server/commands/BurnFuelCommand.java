package edu.arch.server.commands;

import edu.arch.server.domain.interfaces.FuelBurnable;

public class BurnFuelCommand implements Command {
    FuelBurnable fb;

    public BurnFuelCommand(FuelBurnable fb) {
        this.fb = fb;
    }

    @Override
    public void execute() {
        this.fb.setFuel(Math.max(fb.getFuel() - fb.getBurnFuelRate(), 0));
    }
}
