package edu.arch.server.commands;

import edu.arch.server.domain.graph.Direction;
import edu.arch.server.domain.interfaces.Rotable;
import edu.arch.server.exceptions.CommandException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class RotateCommand implements Command {
    private final Logger log = LoggerFactory.getLogger(RotateCommand.class.getName());
    private final Rotable r;
    public RotateCommand(Rotable r) {
        this.r = r;
    }

    @Override
    public void execute() {
        try {
            r.setDirection(Direction.next(r.getDirection(), r.getAngularVelocity()));
            log.info("Rotated to {}", r.getDirection());
        } catch (RuntimeException ex) {
            throw new CommandException(ex);
        }
    }
}
