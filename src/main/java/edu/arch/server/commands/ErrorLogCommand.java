package edu.arch.server.commands;

import edu.arch.server.exceptions.CommandException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ErrorLogCommand implements Command {
    private final Logger log = LoggerFactory.getLogger(ErrorLogCommand.class.getName());
    private final Command cmd;
    private final RuntimeException ex;

    public ErrorLogCommand(Command cmd, CommandException ex) {
        this.cmd = cmd;
        this.ex = ex;
    }

    @Override
    public void execute() {
        log.error("Error occurred on cmd '{}':", cmd, ex);
    }
}
