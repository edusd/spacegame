package edu.arch.server.commands;

public interface Injectable {
    void inject(Command cmd);
}
