package edu.arch.server.commands;

import edu.arch.server.domain.interfaces.FuelBurnable;
import edu.arch.server.exceptions.CommandException;

public class CheckFuelCommand implements Command {
    FuelBurnable fb;

    public CheckFuelCommand(FuelBurnable fb) {
        this.fb = fb;
    }

    @Override
    public void execute() {
        if (fb.getFuel() == 0) {
            throw new CommandException("Топливо закончилось!");
        }
    }
}
