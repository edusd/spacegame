package edu.arch.server.commands;

import edu.arch.server.exceptions.CommandException;

import java.util.List;

public class MacroCommand implements Command {
    List<Command> commands;

    public MacroCommand(List<Command> commands) {
        this.commands = commands;
    }

    @Override
    public void execute() {
        try {
            commands.forEach(Command::execute);
        } catch (CommandException ex) {
            throw ex;
        } catch (RuntimeException ex) {
            throw new CommandException("Непредвиденная ошибка макрокоманды", ex);
        }
    }
}
