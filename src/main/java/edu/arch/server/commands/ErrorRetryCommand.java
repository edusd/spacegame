package edu.arch.server.commands;

import edu.arch.server.ExceptionHandler;
import edu.arch.server.exceptions.CommandException;
import edu.arch.server.exceptions.CommandRetryException;
import edu.arch.server.exceptions.CommandRetryExpireException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ErrorRetryCommand implements Command {
    private final Logger log = LoggerFactory.getLogger(ErrorRetryCommand.class.getName());
    private final Command cmd;
    private final int numberRetry;
    private final int maxAttempts;
    private final ExceptionHandler handler;

    public ErrorRetryCommand(Command cmd, int numberRetry, int maxAttempts, ExceptionHandler handler) {
        this.cmd = cmd;
        this.numberRetry = numberRetry;
        this.maxAttempts = maxAttempts;
        this.handler = handler;
    }

    @Override
    public void execute() {
        try {
            log.info("Retry № {} of cmd '{}'", this.numberRetry, this.cmd);
            cmd.execute();
        } catch (CommandException ex) {
            if (this.numberRetry < this.maxAttempts) {
                handler.handle(this.cmd, new CommandRetryException(ex, this.numberRetry)).execute();
            } else {
                handler.handle(cmd, new CommandRetryExpireException(ex)).execute();
            }
        }
    }
}
