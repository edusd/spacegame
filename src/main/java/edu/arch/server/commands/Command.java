package edu.arch.server.commands;

public interface Command {
    void execute();
}
