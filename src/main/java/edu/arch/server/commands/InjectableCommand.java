package edu.arch.server.commands;

public class InjectableCommand implements Command, Injectable {
    Command cmd;
    public InjectableCommand(Command cmd) {
        this.cmd = cmd;
    }

    @Override
    public void inject(Command cmd) {
        this.cmd = cmd;
    }

    @Override
    public void execute() {
        this.cmd.execute();
    }
}
