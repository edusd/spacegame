package edu.arch.server.commands;

import java.util.Queue;

public class RepeatCommand implements Command {
    Queue<Command> queue;
    Command cmd;

    public RepeatCommand(Queue<Command> queue, Command cmd) {
        this.queue = queue;
        this.cmd = cmd;
    }

    @Override
    public void execute() {
        this.queue.offer(this.cmd);
    }
}
