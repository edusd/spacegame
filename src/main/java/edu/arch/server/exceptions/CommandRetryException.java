package edu.arch.server.exceptions;

public class CommandRetryException extends RuntimeException {
    private final int numberAttempt;

    public CommandRetryException(CommandException ex, int numberAttempt) {
        super(ex);
        this.numberAttempt = numberAttempt;
    }

    public int getNumberAttempt() {
        return this.numberAttempt;
    }
}
