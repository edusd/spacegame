package edu.arch.server.exceptions;

public class CommandRetryExpireException extends RuntimeException {

    public CommandRetryExpireException(CommandException ex) {
        super(ex);
    }
}
