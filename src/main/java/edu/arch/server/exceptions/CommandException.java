package edu.arch.server.exceptions;

public class CommandException extends RuntimeException {
    public CommandException(String message) {
        super(message);
    }
    public CommandException(RuntimeException ex) {
        super(ex);
    }
    public CommandException(String message, RuntimeException ex) {
        super(message, ex);
    }
}
