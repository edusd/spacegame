package edu.arch.server;

import edu.arch.server.domain.graph.Direction;
import edu.arch.server.domain.graph.Vector;
import edu.arch.server.domain.interfaces.Movable;
import edu.arch.server.domain.interfaces.Rotable;

import java.util.function.Consumer;

public class Commands {
    public static final Consumer<Movable> MOVE
            = m -> m.setPosition(Vector.plus(m.getPosition(), m.getVelocity()));
    public static final Consumer<Rotable> ROTATE
            = r -> r.setDirection(Direction.next(r.getDirection(), r.getAngularVelocity()));

    private Commands() {
    }
}
