package edu.arch.server;

import edu.arch.server.commands.BurnFuelCommand;
import edu.arch.server.commands.CheckFuelCommand;
import edu.arch.server.commands.Command;
import edu.arch.server.commands.EmptyCommand;
import edu.arch.server.commands.InjectableCommand;
import edu.arch.server.commands.MacroCommand;
import edu.arch.server.commands.MoveCommand;
import edu.arch.server.commands.RepeatCommand;
import edu.arch.server.commands.RotateCommand;
import edu.arch.server.dao.UObject;
import edu.arch.server.dao.UObjectHashMap;
import edu.arch.server.domain.adapter.FuelBurnableObject;
import edu.arch.server.domain.adapter.MovableObject;
import edu.arch.server.domain.adapter.RotableObject;
import edu.arch.server.exceptions.CommandException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Queue;

public class GameServer {
    private final Logger log = LoggerFactory.getLogger(GameServer.class.getName());
    private final ExceptionHandler exceptionHandler;
    private final List<UObject> objects = new ArrayList<>();

    public GameServer(ExceptionHandler exceptionHandler) {
        this.exceptionHandler = exceptionHandler;
    }

    public void spawn(String name, Map<String, Object> properties) {
        UObject o = new UObjectHashMap();
        o.setProperty("Name", name);
        properties.forEach(o::setProperty);
        this.objects.add(o);
    }

    public Optional<UObject> get(String name) {
        return this.objects.stream().filter(o -> o.getProperty("Name").equals(name)).findFirst();
    }

    public String draw(){
        StringBuilder sb = new StringBuilder();
        this.objects.forEach(uObject -> sb.append(uObject).append("\n"));
        return sb.toString();
    }

    public void movement() {
        this.objects.stream().map(o -> new MacroCommand(List.of(
                new CheckFuelCommand(new FuelBurnableObject(o)),
                new MoveCommand(new MovableObject(o)),
                new BurnFuelCommand(new FuelBurnableObject(o))
        ))).forEach(this::repeatableExecute);
    }

    public void move(){
        this.objects.stream().map(MovableObject::new).map(MoveCommand::new).forEach(this::execute);
    }

    public void rotate(){
        this.objects.stream().map(RotableObject::new).map(RotateCommand::new).forEach(this::execute);
    }

    public void repeatableExecute(Command repeatableCommand) {
        Queue<Command> queue = new LinkedList<>();

        InjectableCommand cmd = new InjectableCommand(repeatableCommand);
        Command repeater = new RepeatCommand(queue, cmd);
        Command macro = new MacroCommand(List.of(repeatableCommand, repeater));
        cmd.inject(macro);

        queue.offer(cmd);
        while (!queue.isEmpty()) {
            try {
                queue.peek().execute();
                queue.remove();
            } catch (CommandException ex) {
                log.info("Выполнение команды прекращено из-за: {}", ex.getMessage());
                cmd.inject(new EmptyCommand());
            }
        }
    }

    private void execute(Command cmd) {
        try {
            cmd.execute();
        } catch (CommandException ex) {
            exceptionHandler.handle(cmd, ex).execute();
        }
    }
}
