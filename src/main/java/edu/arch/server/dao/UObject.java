package edu.arch.server.dao;

public interface UObject {
    Object getProperty(String key);
    void setProperty(String key, Object newValue);
}
