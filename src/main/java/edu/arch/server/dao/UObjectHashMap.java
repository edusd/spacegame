package edu.arch.server.dao;

import java.util.HashMap;

public class UObjectHashMap implements UObject {
    private final HashMap<String, Object> store;

    public UObjectHashMap() {
        this.store = new HashMap<>();
    }

    @Override
    public Object getProperty(String key) {
        return this.store.get(key);
    }

    @Override
    public void setProperty(String key, Object newValue) {
        this.store.put(key, newValue);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        store.forEach((key, value) -> sb.append(key).append(" : ").append(value).append("\n"));
        return sb.toString();
    }
}
