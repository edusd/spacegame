package edu.arch.server;

import edu.arch.server.commands.Command;
import edu.arch.server.commands.ErrorLogCommand;
import edu.arch.server.commands.MoveCommand;
import edu.arch.server.commands.ErrorRetryCommand;
import edu.arch.server.commands.RotateCommand;
import edu.arch.server.exceptions.CommandException;
import edu.arch.server.exceptions.CommandRetryException;
import edu.arch.server.exceptions.CommandRetryExpireException;

import java.util.Map;
import java.util.function.BiFunction;

public class ExceptionHandler {
    private static final int MAX_RETRY_ATTEMPTS = 2;
    private final Map<
            Class<? extends Command>,
            Map<
                    Class<? extends RuntimeException>,
                    BiFunction<Command, RuntimeException, Command>>
            > store;

    private final BiFunction<Command, RuntimeException, Command> firstRetryAttempt =
            (cmd, ex) -> new ErrorRetryCommand(cmd, 1, MAX_RETRY_ATTEMPTS, this);

    private final BiFunction<Command, RuntimeException, Command> retryAttempt =
            (cmd, ex) -> new ErrorRetryCommand(
                    cmd, ((CommandRetryException) ex).getNumberAttempt() + 1, MAX_RETRY_ATTEMPTS, this
            );

    private final BiFunction<Command, RuntimeException, Command> expireRetryAttempt =
            (cmd, ex) -> new ErrorLogCommand(cmd, (CommandException) ex.getCause());

    public ExceptionHandler() {
        store = Map.of(
                RotateCommand.class, Map.of(
                        CommandException.class, this.firstRetryAttempt,
                        CommandRetryException.class, this.retryAttempt,
                        CommandRetryExpireException.class, this.expireRetryAttempt
                ),
                MoveCommand.class, Map.of(
                        CommandException.class, this.firstRetryAttempt,
                        CommandRetryException.class, this.retryAttempt,
                        CommandRetryExpireException.class, this.expireRetryAttempt
                )
        );
    }

    public Command handle(Command cmd, RuntimeException ex) {
        return store.get(cmd.getClass()).get(ex.getClass()).apply(cmd, ex);
    }
}
